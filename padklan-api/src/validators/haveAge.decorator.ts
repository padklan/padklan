import { registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { getAge } from '../utils/date';


export function HaveAge(validationOptions?: ValidationOptions) {
    return (object: any, propertyName: string) => {
        registerDecorator({
            target: object.constructor,
            propertyName,
            options: validationOptions,
            constraints: [],
            validator: HaveAgeConstraint,
        });
    };
}

@ValidatorConstraint({ name: 'Age' })
export class HaveAgeConstraint implements ValidatorConstraintInterface {

    validate(value: any, args: ValidationArguments) {
      return getAge(args.value) >= 13;
    }

    defaultMessage(args: ValidationArguments) {
      return `${args.property} should give 13 years old`;
    }
}