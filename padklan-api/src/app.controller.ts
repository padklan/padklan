import { Controller, Get, Post, Request, Res, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { LocalAuthGuard } from './auth/local-auth.guard'
import { AuthService } from './auth/auth.service';
import { AppService } from './app.service';
import { UsersService } from './users/users.service';
import { UserDto } from './dto/user.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService, private readonly authService: AuthService, private readonly usersService: UsersService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  async getProfile(@Request() req, @Res() res): Promise<UserDto & { _id?: string; }> {
    const user = await this.usersService.findOneByUsername(req.user.username);

    if(user) {
      return res.status(200).json({
        id: user._id,
        username: user.username,
        email: user.email,
        birthdate: user.birthdate,
      });
    }

    return res.status(404).json({
      statusCode: 404,
      message: ['profile not found'],
      error: "Not Found"
    });
  }

  @Post('auth/reset-password')
  async resetPassword(@Request() req) {
    
  }
}
