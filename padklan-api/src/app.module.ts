import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    process.env.NODE_ENV === 'development' ? MongooseModule.forRoot(process.env.DB_URL) 
    : MongooseModule.forRoot(process.env.DB_URL, {
      ssl: true,
      sslValidate: false,
      replicaSet: process.env.DB_REPLICA_SET,
      tlsInsecure: true,
      sslCA: [
        process.env.DB_CA,
      ]
    }), 
    AuthModule, 
    UsersModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
