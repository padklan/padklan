import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';

@Schema({ timestamps: true })
export class User {
  @Prop({ type: mongoose.Schema.Types.String, required: true, unique: true })
  username: string;

  @Prop({ type: mongoose.Schema.Types.String, required: true, unique: true })
  email: string;

  @Prop({ type: mongoose.Schema.Types.String })
  password: string;

  @Prop({ type: mongoose.Schema.Types.Date, required: true })
  birthdate: Date;

  @Prop({ type: mongoose.Schema.Types.String, enum: ['f', 'm'] })
  civility: string;

  @Prop([{ type: mongoose.Schema.Types.String }])
  gamesId: string[];

  @Prop([{ type: mongoose.Schema.Types.String,  enum: ['roleplayer', 'fun', 'calm', 'hardcore', 'casual', 'regular', 'compétitif'] }])
  gamerTypes: string[];

  @Prop({ type: mongoose.Schema.Types.String })
  confirmToken: string;

  @Prop({ type: mongoose.Schema.Types.String })
  resetPasswordToken: string;

  @Prop({ type: mongoose.Schema.Types.Boolean, default: false })
  isActive: boolean;

  @Prop({ type: mongoose.Schema.Types.Boolean, default: false })
  isBan: boolean;

  @Prop({ type: mongoose.Schema.Types.Boolean, default: true })
  acceptNotification: boolean;

  @Prop({ type: mongoose.Schema.Types.Boolean, default: true })
  acceptCGU: boolean;

  @Prop({ type: mongoose.Schema.Types.String })
  pictureUrl: string;
}

export type UserDocument = User & Document;

export const UserSchema = SchemaFactory.createForClass(User);