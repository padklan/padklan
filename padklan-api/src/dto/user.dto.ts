import { IsAlphanumeric, IsEmail, IsEnum, IsNotEmpty, IsOptional, MinLength, MaxLength } from 'class-validator';

import { HaveAge } from '../validators/haveAge.decorator';
import { Match } from '../validators/match.decorator';

export enum Gender {
  Male = 'm',
  Female = 'f'
}

export class CreateUserDto {
  @MinLength(6)
  @MaxLength(16)
  @IsAlphanumeric()
  username: string;

  @IsEmail()
  email: string;

  @MinLength(6)
  @MaxLength(16)
  password: string;

  @MinLength(6)
  @MaxLength(16)
  @Match('password')
  confirmPassword: string;

  @IsNotEmpty()
  @HaveAge()
  birthdate: Date;
}


export class UserDto {
  id: any;
  username: string;
  email: string;
  birthdate: Date;
}

export class UpdateUserDto {
  @IsEnum(Gender)
  @IsOptional()
  civility: string;
}