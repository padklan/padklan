import { Model, Query } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as bcrypt from 'bcrypt';
import { uuid } from 'uuidv4';
import { User, UserDocument } from '../schemas/user.schema';
import { CreateUserDto } from '../dto/user.dto';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async create(createUserDto: CreateUserDto): Promise<User & { _id?: string; }> {
    const user = { 
      username: createUserDto.username,
      email: createUserDto.email,
      password: createUserDto.password,
      birthdate: createUserDto.birthdate,
      confirmToken: uuid(),
    };

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);

    const createdUser = new this.userModel(user);
    return createdUser.save();
  }

  async activeAccount(email: string, confirmToken: string): Promise<Query<any, UserDocument>>  {
    const user = await this.userModel.findOne({ email, confirmToken }).exec();
    if(!user || (user && user.isBan)) {
      return null;
    }

    return this.userModel.updateOne({ email, confirmToken }, { isActive: true, confirmToken: null });
  }

  async findOneByUsername(username: string): Promise<User & { _id?: string; } | undefined> {
    return this.userModel.findOne({ username }).exec();
  }
}
