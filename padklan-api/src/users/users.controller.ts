import { Controller, Body, Delete, Get, Post, Put, Res, Query } from '@nestjs/common';
import * as sendgrid from '@sendgrid/client';
import { UsersService } from './users.service';

import { CreateUserDto, UserDto } from '../dto/user.dto';

@Controller()
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get('users')
  index(): string {
    return 'index';
  }
  
  @Post('users')
  async create(@Body() createUserDto: CreateUserDto, @Res() res): Promise<UserDto | { statusCode: number; message: string[]; error: string; }> {
    try {
      const user = await this.usersService.create(createUserDto);

      sendgrid.setApiKey(process.env.SENDGRID_API_KEY);
      await sendgrid.request({
        method: 'POST',
        url: '/v3/mail/send',
        body: {
          personalizations: [
            {
              to: [
                {
                  "email": user.email
                }
              ],
              dynamic_template_data: {
                username: user.username,
                url: process.env.NODE_ENV === 'development' ? `http://localhost:8080/active-account?confirmToken=${user.confirmToken}&email=${user.email}` : `https://www.padklan.fr/active-account?confirmToken=${user.confirmToken}&email=${user.email}`
              }
            },
          ],
          from: {
            email: 'contact@padklan.fr',
            name: 'Padklan'
          },
          template_id: 'd-c62ad5cc675048689fab60ae1abd9da8'
        }
      });

      return res.status(201).json({
        id: user._id,
        username: user.username,
        email: user.email,
        birthdate: user.birthdate,
      });
    } catch(e) {
      return res.status(409).json({
        statusCode: 409,
        message: [
          e.toString().slice(17, e.toString().length).replace(/:/g, '')
        ],
        error: "Conflict"
      });
    }
  }

  @Post('users/active')
  async active(@Query() query, @Res() res): Promise<any> {
    const { confirmToken, email } = query;
    const user =  await this.usersService.activeAccount(email, confirmToken);

    if(user) {
      return res.status(202).json({
        statusCode: 202,
        message: ['user activated']
      });
    }

    return res.status(404).json({
      statusCode: 404,
      message: ['user not found'],
      error: "Not Found"
    });
  }

  @Get('users/:id')
  show(): any {
    return 'show'
  }

  @Put('users')
  update(): any {

  }

  @Delete('users/:id')
  destroy(): any {

  }
}
