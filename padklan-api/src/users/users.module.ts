import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { User, UserSchema } from '../schemas/user.schema';

@Module({
  imports: [MongooseModule.forFeatureAsync([
    { 
      name: User.name,    
      useFactory: () => {
        const schema = UserSchema;
        schema.plugin(require('mongoose-unique-validator'), { message: 'is not unique' });
        return schema;
      },
   }
  ])],
  providers: [UsersService],
  controllers: [UsersController],
  exports: [UsersService],
})
export class UsersModule {}
