import AddUserModel from '@/models/addUserModel';
import handleResponse from '@/utils/api';

/*eslint class-methods-use-this: "off"*/
class UsersService {
  async register({
    email,
    username,
    password,
    confirmPassword,
    birthdate,
  }: AddUserModel) {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email, username, password, confirmPassword, birthdate,
      }),
    };

    return await handleResponse('/users', requestOptions);
  }

  async activeAccount({ email, confirmToken }: { email: string; confirmToken: string }) {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
    };

    return await handleResponse(`/users/active?confirmToken=${confirmToken}&email=${email}`, requestOptions);
  }
}

export default UsersService;
