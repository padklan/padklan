import Vue from 'vue';
import VueI18n from 'vue-i18n';
import Buefy from 'buefy';
import VueGtag from 'vue-gtag';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faCashRegister,
  faCalendar,
  faCheck,
  faCheckCircle,
  faChevronDown,
  faChevronUp,
  faInfoCircle,
  faExclamationTriangle,
  faExclamationCircle,
  faArrowUp,
  faAngleRight,
  faAngleLeft,
  faAngleDown,
  faEye,
  faEyeSlash,
  faCaretDown,
  faCaretUp,
  faPuzzlePiece,
  faUpload,
  faShieldAlt,
  faSpinner,
} from '@fortawesome/free-solid-svg-icons';
import {
  faDiscord,
  faFacebook,
  faTwitter,
  faYoutube,
  faInstagram,
} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import VueMeta from 'vue-meta';
import App from './App.vue';
import lang from './translations';
import router from './router';
import store from './store';
import currentLanguage from './utils/currentLanguage';
import 'buefy/dist/buefy.css';

Vue.config.productionTip = process.env.NODE_ENV === 'production';

library.add(faCashRegister, faCalendar, faCheck, faCheckCircle, faChevronDown,
  faChevronUp, faInfoCircle, faExclamationTriangle, faExclamationCircle,
  faArrowUp, faAngleRight, faAngleLeft, faAngleDown, faEye, faEyeSlash,
  faCaretDown, faCaretUp, faPuzzlePiece, faUpload, faShieldAlt, faSpinner,
  faDiscord, faFacebook, faTwitter, faYoutube, faInstagram);

Vue.component('vue-fontawesome', FontAwesomeIcon);
Vue.use(Buefy, {
  defaultIconComponent: 'vue-fontawesome',
  defaultIconPack: 'fas',
});

Vue.use(VueMeta, {
  refreshOnceOnNavigation: true,
});

Vue.use(VueI18n);

if (process.env.NODE_ENV === 'production') {
  Vue.use(VueGtag, {
    config: { id: 'UA-86845033-1' },
  });
}

const language = currentLanguage();
new Vue({
  router,
  store,
  render: (h) => h(App),
  mounted: () => document.dispatchEvent(new Event('x-app-rendered')),
  i18n: new VueI18n({
    locale: language,
    fallbackLocale: language,
    messages: {
      fr: lang.fr,
      en: lang.en,
    },
  }),
}).$mount('#app');
