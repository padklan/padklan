export default class AddUserModel {
  username: string;
  email: string;
  password: string;
  confirmPassword: string;
  birthdate: Date | null;

  constructor() {
    this.username = '';
    this.email = '';
    this.password = '';
    this.confirmPassword = '';
    this.birthdate = null;
  }
}
