import AddUserModel from '@/models/addUserModel';
import UsersService from '@/services/usersService';

import { Commit, Dispatch } from '@/types/common';
import router from '../router/index';
import { translateAlert } from '../utils/translateMeta';

const alertMessage = translateAlert('signUpSuccess');

type AccountState = {
  user: AddUserModel | null;
  loading: {
    activeAccount: boolean;
    signUp: boolean;
  };
  errors: {
    signUp: Array<string>;
    activeAccount: boolean | null;
  };
};

const currentUser = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user') as string) : null;
const currentState = currentUser
  ? {
    user: currentUser,
    loading: {
      activeAccount: false,
      signUp: false,
    },
    errors: {
      signUp: [],
      activeAccount: null,
    },
  }
  : {
    user: null,
    loading: {
      activeAccount: false,
      signUp: false,
    },
    errors: {
      signUp: [],
      activeAccount: null,
    },
  };

const usersService = new UsersService();

const actions = {
  async signUp({ dispatch, commit }: { dispatch: Dispatch; commit: Commit }, user: AddUserModel) {
    commit('signUpRequest', user);

    const userResult = await usersService.register(user);

    if (userResult.type === 'error') {
      commit('signUpFailure', userResult.message);
    } else {
      commit('signUpSuccess');
      router.push('/');
      setTimeout(() => {
        dispatch('alert/success', alertMessage, { root: true });
      });
    }
  },
  async activeAccount(
    { commit }: { dispatch: Dispatch; commit: Commit },
    { email, confirmToken }: { email: string; confirmToken: string },
  ) {
    commit('activeAccountRequest');

    const userResult = await usersService.activeAccount({ email, confirmToken });

    if (userResult.type === 'error') {
      commit('activeAccountFailure', userResult.message);
    } else {
      commit('activeAccountSuccess', userResult.data);
    }
  },
};

const mutations = {
  signUpRequest(state: AccountState) {
    state.loading.signUp = true;
  },
  signUpSuccess(state: AccountState) {
    state.loading.signUp = false;
  },
  signUpFailure(state: AccountState, errors: Array<string>) {
    state.errors.signUp = errors;
    state.loading.signUp = false;
  },
  activeAccountRequest(state: AccountState) {
    state.loading.activeAccount = true;
    state.errors.activeAccount = null;
  },
  activeAccountSuccess(state: AccountState) {
    state.loading.activeAccount = false;
    state.errors.activeAccount = false;
  },
  activeAccountFailure(state: AccountState) {
    state.loading.activeAccount = false;
    state.errors.activeAccount = true;
  },
};

const account = {
  namespaced: true,
  state: currentState,
  actions,
  mutations,
};

export default account;
