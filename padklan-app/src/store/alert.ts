import { Commit } from '@/types/common';

type AlertState = {
  alert: {
    type: 'is-success' | 'is-danger' | null;
    message: string | null;
  };
};

const currentState = {
  alert: {
    type: null,
    message: null,
  },
};

const actions = {
  success({ commit }: { commit: Commit }, message: string) {
    commit('success', message);
  },
  error({ commit }: { commit: Commit }, message: string) {
    commit('error', message);
  },
  clear({ commit }: { commit: Commit }) {
    commit('clear');
  },
};

const mutations = {
  success(state: AlertState, message: string) {
    state.alert = {
      type: 'is-success',
      message,
    };
  },
  error(state: AlertState, message: string) {
    state.alert = {
      type: 'is-danger',
      message,
    };
  },
  clear(state: AlertState) {
    state.alert = {
      type: null,
      message: null,
    };
  },
};

const alert = {
  namespaced: true,
  state: currentState,
  actions,
  mutations,
};

export default alert;
