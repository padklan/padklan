async function handleResponse(url: string, requestOptions: RequestInit) {
  const res = await fetch(`${process.env.VUE_APP_API_URL}${url}`, requestOptions);
  const data = JSON.parse(await res.text());

  if (res.status >= 400) {
    return {
      ...data,
      type: 'error',
    };
  }

  return {
    data,
    type: 'success',
  };
}

export default handleResponse;
