export type CurrentLanguageResult = 'en' | 'fr';

const currentLanguage = (): CurrentLanguageResult => {
  // Si en dev navigator language et en prod env params de language par build
  // nginx pour redirection sur francais ou anglais comme build par rapport à la langue dorigine
  let lang = process.env.NODE_ENV === 'development' ? navigator.language.split('-')[0] : process.env.VUE_APP_LANG;

  if (lang !== 'en' && lang !== 'fr') {
    lang = 'en';
  }

  return lang as CurrentLanguageResult;
};

export default currentLanguage;
