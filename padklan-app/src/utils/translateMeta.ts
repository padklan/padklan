import currentLanguage from './currentLanguage';
import locales from '../translations';

export type TranslateMetaResult = {
  title: string;
  description: string;
}
export type ValueParams = 'homepage' | 'aboutpage' | 'contactpage' | 'signuppage' | 'notfoundpage' | 'activeaccountpage' | 'legalnoticepage' | 'cookiesettingspage' | 'privacypolicypage' | 'termsandconditionspage';

export const translateMeta = (value: ValueParams = 'homepage'): TranslateMetaResult => {
  const lang = currentLanguage();

  return {
    title: locales[lang].meta[value].title,
    description: locales[lang].meta[value].description,
  };
};

export type AlertParams = 'signUpSuccess';
export const translateAlert = (value: AlertParams): string => {
  const lang = currentLanguage();

  return locales[lang].alert[value];
};

export default translateMeta;
