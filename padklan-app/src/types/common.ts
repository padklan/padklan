export type Collapse = {
  title: string;
  text: string;
}

/*eslint @typescript-eslint/no-explicit-any: "off"*/
export type Dispatch = (message: string, data?: any, option?: { root?: boolean }) => void;

/*eslint @typescript-eslint/no-explicit-any: "off"*/
export type Commit = (message: string, data?: any) => void;
