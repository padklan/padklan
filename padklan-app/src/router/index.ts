import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue'),
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
  // {
  //   path: '/contact',
  //   name: 'Contact',
  //   component: () => import(/* webpackChunkName: "contact" */ '../views/Contact.vue'),
  // },
  {
    path: '/legal-notice',
    name: 'Legal Notice',
    component: () => import(/* webpackChunkName: "legalnotice" */ '../views/LegalNotice.vue'),
  },
  {
    path: '/cookie-settings',
    name: 'Cookie Settings',
    component: () => import(/* webpackChunkName: "cookiesettings" */ '../views/CookieSettings.vue'),
  },
  {
    path: '/privacy-policy',
    name: 'Privacy Policy',
    component: () => import(/* webpackChunkName: "privacypolicy" */ '../views/PrivacyPolicy.vue'),
  },
  {
    path: '/terms-and-conditions',
    name: 'Terms And Conditions',
    component: () => import(/* webpackChunkName: "privacypolicy" */ '../views/TermsAndConditions.vue'),
  },
  {
    path: '/sign-up',
    name: 'Sign up',
    component: () => import(/* webpackChunkName: "signup" */ '../views/SignUp.vue'),
  },
  {
    path: '/active-account',
    name: 'Active Account',
    component: () => import(/* webpackChunkName: "activeaccount" */ '../views/ActiveAccount.vue'),
  },
  {
    path: '*',
    name: 'Not Found',
    component: () => import(/* webpackChunkName: "notfound" */ '../views/NotFound.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: '/',
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});

export default router;
