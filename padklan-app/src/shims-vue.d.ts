declare module '*.vue' {
  import Vue from 'vue';

  export default Vue;
}

declare module 'vue-cookieconsent-component' {
  import * as CookieConsent from 'vue-cookieconsent-component';

  export default CookieConsent;
}