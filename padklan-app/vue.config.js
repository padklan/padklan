module.exports = {
  devServer: {
    disableHostCheck: true,
  },
  pluginOptions: {
    prerenderSpa: {
      registry: undefined,
      renderRoutes: [
        '/',
        '/about',
        // '/contact',
        '/sign-up',
      ],
      useRenderEvent: true,
      headless: true,
      onlyProduction: true,
    },
  },
};
