# padklan

## Requirements
- Docker
- Docker Compose
- Node 12.16.1

## Project setup
```
mkdir data
docker-compose build --no-cache
```

### Compiles and hot-reloads for development
```
docker-compose up
```

### Delete container and images for development
```
docker-compose stop
docker-compose rm
docker-compose down
```